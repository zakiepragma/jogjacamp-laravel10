@extends('layouts.app')
@section('title', 'Categories')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-5">
                <div class="card">
                    <div class="card-header">Categories</div>
                    <div class="card-body">
                        <form action="{{ route('categories.index') }}" method="GET">
                            <div class="d-flex justify-content-center mb-4">
                                <input type="text" class="form-control mr-4" name="search" value="{{ request('search') }}" placeholder="Search...">
                                <button type="submit" class="btn btn-primary mr-4"><i class="fas fa-search"></i></button>
                                <a href="{{ route('categories.create') }}" class="btn btn-success"><i class="fas fa-add"></i></a>
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Is Publish</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->is_publish ? 'Yes' : 'No' }}</td>
                                        <td>{{ $category->created_at }}</td>
                                        <td>{{ $category->updated_at }}</td>
                                        <td>
                                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                            <a href="{{ route('categories.show', $category->id) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                            <form action="{{ route('categories.destroy', $category->id) }}" method="POST" style="display: inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center">
                            {{ $categories->links('pagination::bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection